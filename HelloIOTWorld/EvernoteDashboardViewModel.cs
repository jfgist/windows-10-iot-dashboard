﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Evernote.EDAM.Type;

namespace HelloIOTWorld
{
    internal class EvernoteDashboardViewModel :INotifyPropertyChanged
    {
        public ICommand ListByNotebookCommand { protected set; get; }
        public ICommand ListByTagsCommand { protected set; get; }

        public string Status { get; set; }
        private List<Note> _notes;

        public List<Note> Notes
        {
            get { return _notes; }
            set
            {
                _notes = value; 
                OnPropertyChanged();
            }
        }

        private readonly NoteModel _noteModel = new NoteModel();

        public EvernoteDashboardViewModel()
        {
            ListByNotebookCommand = new DelegateCommand(ExecuteListByNotebookCommand);
            ListByTagsCommand = new DelegateCommand(ExecuteListByTagsCommand);
            _noteModel.Authenticate();
            Status = _noteModel.Status;
        }

        /// <summary>
        /// Pass the method a Notebook and list all of the Notes in that Notebook
        /// </summary>
        /// <param name="param"></param>
        public void ExecuteListByNotebookCommand(object param)
        {
            Notes = _noteModel.ListAllNotes((Notebook)param);
        }

        public void ExecuteListByTagsCommand(object param)
        {
            _noteModel.FilterByTags((List<string>) param);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}