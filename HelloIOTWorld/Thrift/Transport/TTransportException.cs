﻿using System;

namespace HelloIOTWorld.Thrift.Transport
{
    public class TTransportException : Exception
    {
        protected ExceptionType type;

        public TTransportException()
            : base()
        {
        }

        public TTransportException(ExceptionType type)
            : this()
        {
            this.type = type;
        }

        public TTransportException(ExceptionType type, string message)
            : base(message)
        {
            this.type = type;
        }

        public TTransportException(string message)
            : base(message)
        {
        }

        public ExceptionType Type
        {
            get { return type; }
        }

        public enum ExceptionType
        {
            Unknown,
            NotOpen,
            AlreadyOpen,
            TimedOut,
            EndOfFile
        }
    }
}
