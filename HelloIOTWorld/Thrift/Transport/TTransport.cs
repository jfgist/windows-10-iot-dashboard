﻿using System;

namespace HelloIOTWorld.Thrift.Transport
{
    public abstract class TTransport
    {
        public abstract bool IsOpen
        {
            get;
        }

        public bool Peek()
        {
            return IsOpen;
        }

        public abstract void Open();

        public abstract void Close();

        public abstract int Read(byte[] buf, int off, int len);

        public int ReadAll(byte[] buf, int off, int len)
        {
            int got = 0;
            int ret = 0;

            while (got < len)
            {
                ret = Read(buf, off + got, len - got);
                if (ret <= 0)
                {
                    throw new TTransportException("Cannot read, Remote side has closed");
                }
                got += ret;
            }

            return got;
        }

        public virtual void Write(byte[] buf)
        {
            Write(buf, 0, buf.Length);
        }

        public abstract void Write(byte[] buf, int off, int len);

        public virtual void Flush()
        {
        }

        public virtual IAsyncResult BeginFlush(AsyncCallback callback, object state)
        {
            return null;
        }

        public virtual void EndFlush(IAsyncResult asyncResult)
        {
        }
    }
}
