﻿using System;
using System.Collections.Generic;
using System.Linq;
using Evernote.EDAM.NoteStore;
using Evernote.EDAM.Type;
using Evernote.EDAM.UserStore;
using HelloIOTWorld.Thrift.Protocol;
using HelloIOTWorld.Thrift.Transport;
using Thrift.Protocol;
using Constants = Evernote.EDAM.UserStore.Constants;

namespace HelloIOTWorld
{
    internal class NoteModel
    {
        public string Status { get; private set; }
        private NoteStore.Client _noteStore;
        private string _authToken;

        public void Authenticate()
        {
            // Real applications authenticate with Evernote using OAuth, but for the
            // purpose of exploring the API, you can get a developer token that allows
            // you to access your own Evernote account. To get a developer token, visit 
            // https://sandbox.evernote.com/api/DeveloperToken.action
            _authToken = "S=s1:U=92fcf:E=15f019edb94:C=157a9edaf88:P=1cd:A=en-devtoken:V=2:H=c7181d7d1ac811db42f46b82783f69d8";

            if (_authToken == "your developer token")
            {
                Status = "Please fill in your developer token";
                return;
            }

            // Initial development is performed on our sandbox server. To use the production 
            // service, change "sandbox.evernote.com" to "www.evernote.com" and replace your
            // developer token above with a token from 
            // https://www.evernote.com/api/DeveloperToken.action
            String evernoteHost = "sandbox.evernote.com";

            Uri userStoreUrl = new Uri("https://" + evernoteHost + "/edam/user");
            TTransport userStoreTransport = new THttpClient(userStoreUrl);
            TProtocol userStoreProtocol = new TBinaryProtocol(userStoreTransport);
            UserStore.Client userStore = new UserStore.Client(userStoreProtocol);

            bool versionOk =
                userStore.checkVersion("Evernote EDAMTest (C#)",
                   Constants.EDAM_VERSION_MAJOR,
                   Constants.EDAM_VERSION_MINOR);
            Status = "Is my Evernote API version up to date? " + versionOk;
            if (!versionOk)
            {
                return;
            }
            // Get the URL used to interact with the contents of the user's account
            // When your application authenticates using OAuth, the NoteStore URL will
            // be returned along with the auth token in the final OAuth request.
            // In that case, you don't need to make this call.
            string noteStoreUrl = userStore.getNoteStoreUrl(_authToken);

            TTransport noteStoreTransport = new THttpClient(new Uri(noteStoreUrl));
            TProtocol noteStoreProtocol = new TBinaryProtocol(noteStoreTransport);
            _noteStore = new NoteStore.Client(noteStoreProtocol);
        }

        public List<Notebook> ListAllNotebooks()
        {
              return _noteStore.listNotebooks(_authToken);
        }

        public List<Note> ListAllNotes(Notebook notebook)
        {
            NoteFilter filter = new NoteFilter();
            filter.NotebookGuid = notebook.Guid;
            return _noteStore.findNotes(_authToken, filter, 0, 100).Notes;
        }

        public List<Note> FilterByTags(List<string> tags)
        {
            var filter=new NoteFilter();
            filter.TagGuids = TagnamesToGuids(tags);

            return _noteStore.findNotes(_authToken, filter, 0, 100).Notes;
        }

        private List<string> TagnamesToGuids(List<string> tagNames)
        {
            var tags = _noteStore.listTags(_authToken);
            var tagGuids = new List<string>();

            foreach (var tagName in tagNames)
            {
                var guid = tags.FirstOrDefault(t => t.Name == tagName).Guid;
                tagGuids.Add(guid);
            }

            return tagGuids;
        }

    }
}
