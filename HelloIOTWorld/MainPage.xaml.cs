﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloIOTWorld
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            DataContext = new EvernoteDashboardViewModel();
        }

//        private void ClickMe_Click(object sender, RoutedEventArgs e)
//        {
            #region

/*
                                    HelloMessage.Text = "XXX";
                        
                        
                                    // Real applications authenticate with Evernote using OAuth, but for the
                                    // purpose of exploring the API, you can get a developer token that allows
                                    // you to access your own Evernote account. To get a developer token, visit 
                                    // https://sandbox.evernote.com/api/DeveloperToken.action
                                    String authToken = "S=s1:U=92fcf:E=15f019edb94:C=157a9edaf88:P=1cd:A=en-devtoken:V=2:H=c7181d7d1ac811db42f46b82783f69d8";
                        
                                    if (authToken == "your developer token")
                                    {
                                        HelloMessage.Text = "Please fill in your developer token";
                                        return;
                                    }
                                    
                                    // Initial development is performed on our sandbox server. To use the production 
                                    // service, change "sandbox.evernote.com" to "www.evernote.com" and replace your
                                    // developer token above with a token from 
                                    // https://www.evernote.com/api/DeveloperToken.action
                                    String evernoteHost = "sandbox.evernote.com";
                        
                                    Uri userStoreUrl = new Uri("https://" + evernoteHost + "/edam/user");
                                    TTransport userStoreTransport = new THttpClient(userStoreUrl);
                                    TProtocol userStoreProtocol = new TBinaryProtocol(userStoreTransport);
                                    UserStore.Client userStore = new UserStore.Client(userStoreProtocol);
                        
                                    bool versionOK =
                                        userStore.checkVersion("Evernote EDAMTest (C#)",
                                           Evernote.EDAM.UserStore.Constants.EDAM_VERSION_MAJOR,
                                           Evernote.EDAM.UserStore.Constants.EDAM_VERSION_MINOR);
                                    HelloMessage.Text= "Is my Evernote API version up to date? " + versionOK;
                                    if (!versionOK)
                                    {
                                        return;
                                    }
                        */
            // Get the URL used to interact with the contents of the user's account
            // When your application authenticates using OAuth, the NoteStore URL will
            // be returned along with the auth token in the final OAuth request.
            // In that case, you don't need to make this call.
            /*
                       String noteStoreUrl = userStore.getNoteStoreUrl(authToken);

                       TTransport noteStoreTransport = new THttpClient(new Uri(noteStoreUrl));
                       TProtocol noteStoreProtocol = new TBinaryProtocol(noteStoreTransport);
                       NoteStore.Client noteStore = new NoteStore.Client(noteStoreProtocol);

                       // List all of the notebooks in the user's account        
                       List<Notebook> notebooks = noteStore.listNotebooks(authToken);
                       //Console.WriteLine("Found " + notebooks.Count + " notebooks:");
                       foreach (Notebook notebook in notebooks)
                       {
                           //Console.WriteLine("  * " + notebook.Name);
                           ListBox.Items.Add(notebook.Name);
                           NoteFilter filter = new NoteFilter();
                           filter.NotebookGuid = notebook.Guid;
                           var notes = noteStore.findNotes(authToken, filter, 0, 100).Notes;
                           foreach (var note in notes)
                           {
                               ListBox.Items.Add(" -" +note.Title);
                           }

                           }*/

            #endregion
//        }
    }
}

